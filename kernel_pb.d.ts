// package: 
// file: kernel.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as google_protobuf_duration_pb from "google-protobuf/google/protobuf/duration_pb";

export class Content extends jspb.Message { 
    getType(): string;
    setType(value: string): Content;
    getValue(): Uint8Array | string;
    getValue_asU8(): Uint8Array;
    getValue_asB64(): string;
    setValue(value: Uint8Array | string): Content;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Content.AsObject;
    static toObject(includeInstance: boolean, msg: Content): Content.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Content, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Content;
    static deserializeBinaryFromReader(message: Content, reader: jspb.BinaryReader): Content;
}

export namespace Content {
    export type AsObject = {
        type: string,
        value: Uint8Array | string,
    }
}

export class Error extends jspb.Message { 
    getMessage(): string;
    setMessage(value: string): Error;

    hasStack(): boolean;
    clearStack(): void;
    getStack(): string | undefined;
    setStack(value: string): Error;

    hasPosition(): boolean;
    clearPosition(): void;
    getPosition(): Error.LineAndColumn | undefined;
    setPosition(value?: Error.LineAndColumn): Error;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Error.AsObject;
    static toObject(includeInstance: boolean, msg: Error): Error.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Error, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Error;
    static deserializeBinaryFromReader(message: Error, reader: jspb.BinaryReader): Error;
}

export namespace Error {
    export type AsObject = {
        message: string,
        stack?: string,
        position?: Error.LineAndColumn.AsObject,
    }


    export class LineAndColumn extends jspb.Message { 
        getLine(): number;
        setLine(value: number): LineAndColumn;
        getColumn(): number;
        setColumn(value: number): LineAndColumn;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): LineAndColumn.AsObject;
        static toObject(includeInstance: boolean, msg: LineAndColumn): LineAndColumn.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: LineAndColumn, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): LineAndColumn;
        static deserializeBinaryFromReader(message: LineAndColumn, reader: jspb.BinaryReader): LineAndColumn;
    }

    export namespace LineAndColumn {
        export type AsObject = {
            line: number,
            column: number,
        }
    }

}

export class VSCode extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): VSCode.AsObject;
    static toObject(includeInstance: boolean, msg: VSCode): VSCode.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: VSCode, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): VSCode;
    static deserializeBinaryFromReader(message: VSCode, reader: jspb.BinaryReader): VSCode;
}

export namespace VSCode {
    export type AsObject = {
    }


    export class ReadCache extends jspb.Message { 
        getKey(): string;
        setKey(value: string): ReadCache;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): ReadCache.AsObject;
        static toObject(includeInstance: boolean, msg: ReadCache): ReadCache.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: ReadCache, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): ReadCache;
        static deserializeBinaryFromReader(message: ReadCache, reader: jspb.BinaryReader): ReadCache;
    }

    export namespace ReadCache {
        export type AsObject = {
            key: string,
        }
    }

    export class WriteCache extends jspb.Message { 
        getKey(): string;
        setKey(value: string): WriteCache;

        hasValue(): boolean;
        clearValue(): void;
        getValue(): string | undefined;
        setValue(value: string): WriteCache;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): WriteCache.AsObject;
        static toObject(includeInstance: boolean, msg: WriteCache): WriteCache.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: WriteCache, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): WriteCache;
        static deserializeBinaryFromReader(message: WriteCache, reader: jspb.BinaryReader): WriteCache;
    }

    export namespace WriteCache {
        export type AsObject = {
            key: string,
            value?: string,
        }
    }

    export class Cached extends jspb.Message { 
        getKey(): string;
        setKey(value: string): Cached;

        hasValue(): boolean;
        clearValue(): void;
        getValue(): string | undefined;
        setValue(value: string): Cached;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Cached.AsObject;
        static toObject(includeInstance: boolean, msg: Cached): Cached.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Cached, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Cached;
        static deserializeBinaryFromReader(message: Cached, reader: jspb.BinaryReader): Cached;
    }

    export namespace Cached {
        export type AsObject = {
            key: string,
            value?: string,
        }
    }

    export class Prompt extends jspb.Message { 

        hasPrompt(): boolean;
        clearPrompt(): void;
        getPrompt(): string | undefined;
        setPrompt(value: string): Prompt;

        hasPlaceholder(): boolean;
        clearPlaceholder(): void;
        getPlaceholder(): string | undefined;
        setPlaceholder(value: string): Prompt;
        getSecret(): boolean;
        setSecret(value: boolean): Prompt;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Prompt.AsObject;
        static toObject(includeInstance: boolean, msg: Prompt): Prompt.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Prompt, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Prompt;
        static deserializeBinaryFromReader(message: Prompt, reader: jspb.BinaryReader): Prompt;
    }

    export namespace Prompt {
        export type AsObject = {
            prompt?: string,
            placeholder?: string,
            secret: boolean,
        }
    }

    export class Prompted extends jspb.Message { 

        hasValue(): boolean;
        clearValue(): void;
        getValue(): string | undefined;
        setValue(value: string): Prompted;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Prompted.AsObject;
        static toObject(includeInstance: boolean, msg: Prompted): Prompted.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Prompted, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Prompted;
        static deserializeBinaryFromReader(message: Prompted, reader: jspb.BinaryReader): Prompted;
    }

    export namespace Prompted {
        export type AsObject = {
            value?: string,
        }
    }

}

export class Eval extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Eval.AsObject;
    static toObject(includeInstance: boolean, msg: Eval): Eval.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Eval, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Eval;
    static deserializeBinaryFromReader(message: Eval, reader: jspb.BinaryReader): Eval;
}

export namespace Eval {
    export type AsObject = {
    }


    export class Code extends jspb.Message { 
        getCode(): string;
        setCode(value: string): Code;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Code.AsObject;
        static toObject(includeInstance: boolean, msg: Code): Code.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Code, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Code;
        static deserializeBinaryFromReader(message: Code, reader: jspb.BinaryReader): Code;
    }

    export namespace Code {
        export type AsObject = {
            code: string,
        }
    }

    export class Cancel extends jspb.Message { 

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Cancel.AsObject;
        static toObject(includeInstance: boolean, msg: Cancel): Cancel.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Cancel, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Cancel;
        static deserializeBinaryFromReader(message: Cancel, reader: jspb.BinaryReader): Cancel;
    }

    export namespace Cancel {
        export type AsObject = {
        }
    }

    export class Output extends jspb.Message { 
        clearContentList(): void;
        getContentList(): Array<Content>;
        setContentList(value: Array<Content>): Output;
        addContent(value?: Content, index?: number): Content;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Output.AsObject;
        static toObject(includeInstance: boolean, msg: Output): Output.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Output, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Output;
        static deserializeBinaryFromReader(message: Output, reader: jspb.BinaryReader): Output;
    }

    export namespace Output {
        export type AsObject = {
            contentList: Array<Content.AsObject>,
        }
    }

    export class Result extends jspb.Message { 

        hasDuration(): boolean;
        clearDuration(): void;
        getDuration(): google_protobuf_duration_pb.Duration | undefined;
        setDuration(value?: google_protobuf_duration_pb.Duration): Result;
        clearErrorsList(): void;
        getErrorsList(): Array<Error>;
        setErrorsList(value: Array<Error>): Result;
        addErrors(value?: Error, index?: number): Error;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Result.AsObject;
        static toObject(includeInstance: boolean, msg: Result): Result.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Result, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Result;
        static deserializeBinaryFromReader(message: Result, reader: jspb.BinaryReader): Result;
    }

    export namespace Result {
        export type AsObject = {
            duration?: google_protobuf_duration_pb.Duration.AsObject,
            errorsList: Array<Error.AsObject>,
        }
    }

}
